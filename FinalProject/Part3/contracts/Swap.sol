// SPDX-License-Identifier: MIT
pragma solidity >=0.8.0 <0.9.0;
import "@openzeppelin/contracts/access/Ownable.sol";
import "./interfaces/ISwap.sol";
import "./sAsset.sol";

contract Swap is Ownable, ISwap {

    address token0;
    address token1;
    uint reserve0;
    uint reserve1;
    mapping (address => uint) shares;
    uint public totalShares;

    constructor(address addr0, address addr1) {
        token0 = addr0;
        token1 = addr1;
    }

    function init(uint token0Amount, uint token1Amount) external override onlyOwner {
        require(reserve0 == 0 && reserve1 == 0, "init - already has liquidity");
        require(token0Amount > 0 && token1Amount > 0, "init - both tokens are needed");
        
        require(sAsset(token0).transferFrom(msg.sender, address(this), token0Amount));
        require(sAsset(token1).transferFrom(msg.sender, address(this), token1Amount));
        reserve0 = token0Amount;
        reserve1 = token1Amount;
        totalShares = sqrt(token0Amount * token1Amount);
        shares[msg.sender] = totalShares;
    }

    // https://github.com/Uniswap/v2-core/blob/v1.0.1/contracts/libraries/Math.sol
    function sqrt(uint y) internal pure returns (uint z) {
        if (y > 3) {
            z = y;
            uint x = y / 2 + 1;
            while (x < z) {
                z = x;
                x = (y / x + x) / 2;
            }
        } else if (y != 0) {
            z = 1;
        }
    }

    function getReserves() external view returns (uint, uint) {
        return (reserve0, reserve1);
    }

    function getTokens() external view returns (address, address) {
        return (token0, token1);
    }

    function getShares(address LP) external view returns (uint) {
        return shares[LP];
    }

    /* TODO: implement your functions here */
    /* addLiquidity is used by future liquidity providers to deposit tokens, and will generate new shares based on the token amount in the deposit w.r.t. the pool. 
       Adding liquidity requires an equivalent value of two tokens. 
       Callers need to specify the amount of token 0 they want to deposit (amount0) 
       and the amount of token 1 required to be added (amount1) is determined using the reserve rate at the moment of their deposit, 
       i.e.,amount1 = reserve1 * amount0 / reserve0. And the amount of shares received by the liquidity provider is: new_shares = total_shares * amount0 / reserve0. */
    function addLiquidity(uint token0Amount) override external {
        // To maintain the original ratio of reserve1/reserve0, calculate token1Amount (the amount of token 1 required to be added).
        uint token1Amount = reserve1 * token0Amount / reserve0;
        uint new_shares = totalShares * token0Amount / reserve0;
        require(sAsset(token0).transferFrom(msg.sender, address(this), token0Amount));
        require(sAsset(token1).transferFrom(msg.sender, address(this), token1Amount));
        totalShares += new_shares; // update totalShares
        
        // update reserve0 and reserve1
        reserve0 += token0Amount;
        reserve1 += token1Amount;

        shares[msg.sender] += new_shares; // update shares[msg.sender]
        
    }

    function removeLiquidity(uint withdrawShares) override external {
        uint sender_shares = shares[msg.sender];
        require(sender_shares >= withdrawShares, "sender_shares < withdrawShares");
        uint amount0 = reserve0 * withdrawShares / totalShares;
        uint amount1 = reserve1 * withdrawShares / totalShares;
        // require(amount0 != reserve0, "TEST REQUIRE: amount0 == reserve0");
        // require(reserve0 == sAsset(token0).balanceOf(address(this)), "NOT EQUAL");
        require(sAsset(token0).balanceOf(address(this)) >= amount0, "contract has insufficient token 0");
        require(sAsset(token1).balanceOf(address(this)) >= amount1, "contract has insufficient token 1"); 
        require(sAsset(token0).transfer(msg.sender, amount0));
        require(sAsset(token1).transfer(msg.sender, amount1));
        
        
        // update reserve0 and reserve1
        reserve0 -= amount0;
        reserve1 -= amount1;

        // require(sAsset(token0).balanceOf(address(this)) == reserve0, "reserve0 has incorrect value.");
        // require(sAsset(token1).balanceOf(address(this)) == reserve1, "reserve1 has incorrect value."); 
        
        // update shares[msg.sender]
        shares[msg.sender] -= withdrawShares;
    }

    function token0To1(uint token0Amount) override external {
        // Check if contract has correct balance of token0 and token1 before transfer
        // require(sAsset(token0).balanceOf(address(this)) == reserve0, "reserve0 has incorrect value.");
        // require(sAsset(token1).balanceOf(address(this)) == reserve1, "reserve1 has incorrect value."); 
        
        // uint protocol_fee = token0Amount * 3 / 1000;
        uint token0_to_exchange = token0Amount * 997 / 1000;
        uint token1_to_return = reserve1 - (reserve0 * reserve1) / (reserve0 + token0_to_exchange);
        
        require(sAsset(token0).transferFrom(msg.sender, address(this), token0Amount));
        require(sAsset(token1).transfer(msg.sender, token1_to_return));
        
        // Update reserve0 and reserve1
        reserve0 += token0Amount;
        reserve1 -= token1_to_return;

        // Check if contract has correct balance of token0 and token1 after transfer
        // require(sAsset(token0).balanceOf(address(this)) == reserve0, "reserve0 has incorrect value.");
        // require(sAsset(token1).balanceOf(address(this)) == reserve1, "reserve1 has incorrect value."); 
    }

    function token1To0(uint token1Amount) override external {
        // Check if contract has correct balance of token0 and token1 before transfer
        // require(sAsset(token0).balanceOf(address(this)) == reserve0, "reserve0 has incorrect value.");
        // require(sAsset(token1).balanceOf(address(this)) == reserve1, "reserve1 has incorrect value."); 
        
        // uint protocol_fee = token1Amount * 3 / 1000;
        uint token1_to_exchange = token1Amount * 997 / 1000;
        uint token0_to_return = reserve0 - (reserve1 * reserve0) / (reserve1 + token1_to_exchange);
        
        require(sAsset(token1).transferFrom(msg.sender, address(this), token1Amount));
        require(sAsset(token0).transfer(msg.sender, token0_to_return));
        // Update reserve0 and reserve1
        reserve1 += token1Amount;
        reserve0 -= token0_to_return;

        // Check if contract has correct balance of token0 and token1 after transfer
        // require(sAsset(token0).balanceOf(address(this)) == reserve0, "reserve0 has incorrect value.");
        // require(sAsset(token1).balanceOf(address(this)) == reserve1, "reserve1 has incorrect value."); 
    }
    

    
}