// SPDX-License-Identifier: MIT
pragma solidity >=0.8.0 <0.9.0;

import "@openzeppelin/contracts/access/Ownable.sol";
import "./interfaces/IPriceFeed.sol";
import "./interfaces/IMint.sol";
import "./sAsset.sol";
import "./EUSD.sol";

// import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
// import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract Mint is Ownable, IMint{

    struct Asset {
        address token;
        uint minCollateralRatio;
        address priceFeed;
    }

    struct Position {
        uint idx;
        address owner;
        uint collateralAmount;
        address assetToken;
        uint assetAmount;
    }

    mapping(address => Asset) _assetMap;
    uint _currentPositionIndex;
    mapping(uint => Position) _idxPositionMap;
    address public collateralToken;
    

    constructor(address collateral) {
        collateralToken = collateral;
    }

    function registerAsset(address assetToken, uint minCollateralRatio, address priceFeed) external override onlyOwner {
        require(assetToken != address(0), "Invalid assetToken address");
        require(minCollateralRatio >= 1, "minCollateralRatio must be greater than 100%");
        require(_assetMap[assetToken].token == address(0), "Asset was already registered");
        
        _assetMap[assetToken] = Asset(assetToken, minCollateralRatio, priceFeed);
    }

    function getPosition(uint positionIndex) external view returns (address, uint, address, uint) {
        require(positionIndex < _currentPositionIndex, "Invalid index");
        Position storage position = _idxPositionMap[positionIndex];
        return (position.owner, position.collateralAmount, position.assetToken, position.assetAmount);
    }

    function getMintAmount(uint collateralAmount, address assetToken, uint collateralRatio) public view returns (uint) {
        Asset storage asset = _assetMap[assetToken];
        (int relativeAssetPrice, ) = IPriceFeed(asset.priceFeed).getLatestPrice();
        uint8 decimal = sAsset(assetToken).decimals();
        uint mintAmount = collateralAmount * (10 ** uint256(decimal)) / uint(relativeAssetPrice) / collateralRatio ;
        return mintAmount;
    }

    function checkRegistered(address assetToken) public view returns (bool) {
        return _assetMap[assetToken].token == assetToken;
    }

    /* TODO: implement your functions here */
    /* Create a new position by transferring collateralAmount EUSD tokens from the message sender to the contract. 
       Make sure the asset is registered and the input collateral ratio is not less than the asset MCR, then calculate the number of minted tokens to send to the message sender. */
    function openPosition(uint collateralAmount, address assetToken, uint collateralRatio) override external {
        
        require(this.checkRegistered(assetToken), "checkRegistered failed");
        require(collateralRatio >= _assetMap[assetToken].minCollateralRatio, "the input collateral ratio < MCR");
        
        // Transfer collateralAmount EUSD tokens from the msg.sender to the contract
        EUSD(collateralToken).transferFrom(msg.sender, address(this), collateralAmount);

        // Calculate the number of minted tokens
        uint assetAmount = this.getMintAmount(collateralAmount, assetToken, collateralRatio);

        // Create new position
        _idxPositionMap[_currentPositionIndex] = Position(_currentPositionIndex, msg.sender, collateralAmount, assetToken, assetAmount);
        sAsset(assetToken).mint(address(msg.sender), assetAmount);
        _currentPositionIndex += 1;

    }
    
    /* Close a position when the position owner calls this function. 
       Transfer the sAsset tokens from the message sender to the contract and burn these tokens. 
       Transfer EUSD tokens locked in the position to the message sender. 
       Finally, delete the position at the given index. */
    function closePosition(uint positionIndex) override external {
        require(positionIndex >= 0, "invalid positionIndex");
        (address owner, uint collateralAmount, address assetToken, uint assetAmount) = this.getPosition(positionIndex);
        require(owner == msg.sender, "owner is not as same as msg.sender");
        
        // directly burn the assetAmount
        sAsset(assetToken).burn(address(msg.sender), assetAmount);
        require(EUSD(collateralToken).balanceOf(address(this)) >= collateralAmount, "contract has insufficient collateral token"); 
        // EUSD(collateralToken).transferFrom(address(this), owner, collateralAmount); // We did not call approve first in test.js
        EUSD(collateralToken).transfer(owner, collateralAmount);
        delete _idxPositionMap[positionIndex];
    }

    /* Add collateral amount of the position at the given index. 
       Make sure the message sender owns the position and transfer deposited tokens from the sender to the contract. */
    function deposit(uint positionIndex, uint collateralAmount) override external {
        require(positionIndex >= 0, "invalid positionIndex");
        (address owner, uint cur_collateralAmount, address assetToken, uint assetAmount) = this.getPosition(positionIndex);
        require(owner == msg.sender, "owner is not as same as msg.sender");
        require(EUSD(collateralToken).balanceOf(owner) >= collateralAmount, "owner has insufficient collateral token"); 
        // deposit EUSD from owner to contract
        EUSD(collateralToken).transferFrom(owner, address(this), collateralAmount);
        // update position
        uint new_collateralAmount = cur_collateralAmount + collateralAmount;
        _idxPositionMap[positionIndex].collateralAmount = new_collateralAmount;
    }

    /* Withdraw collateral tokens from the position at the given index. 
       Make sure the message sender owns the position and the collateral ratio won't go below the MCR. 
       Transfer withdrawn tokens from the contract to the sender. */
    function withdraw(uint positionIndex, uint withdrawAmount) override external {
        // check valid positionIndex
        require(positionIndex >= 0, "invalid positionIndex");
        (address owner, uint cur_collateralAmount, address assetToken, uint assetAmount) = this.getPosition(positionIndex);
        // check if msg.sender owns the position
        require(owner == msg.sender, "owner is not as same as msg.sender");
        
        // Calculate new ratio
        Asset storage asset = _assetMap[assetToken];
        (int relativeAssetPrice, ) = IPriceFeed(asset.priceFeed).getLatestPrice();
        uint8 decimal = sAsset(assetToken).decimals();

        int new_collateralAmount = int(cur_collateralAmount) - int(withdrawAmount);
        require(new_collateralAmount >= 0, "new_collateralAmount < 0");
        uint new_ratio = (uint(new_collateralAmount) * (10 ** uint256(decimal))) / assetAmount / uint(relativeAssetPrice);
        
        // check if new ratio is larger than MRC
        require(new_ratio >= _assetMap[assetToken].minCollateralRatio, "new ratio is below MRC");
        // withdraw EUSD from contract to owner
        // EUSD(collateralToken).transferFrom(address(this), owner, withdrawAmount);
        EUSD(collateralToken).transfer(owner, withdrawAmount);
        // update position
        _idxPositionMap[positionIndex].collateralAmount = uint(new_collateralAmount);  
    }
    
    /* Mint more asset tokens from the position at the given index. 
       Make sure the message sender owns the position 
       and the collateral ratio won't go below the MCR */
    function mint(uint positionIndex, uint mintAmount) override external {
        require(positionIndex >= 0, "invalid positionIndex");
        (address owner, uint cur_collateralAmount, address assetToken, uint assetAmount) = this.getPosition(positionIndex);
        require(owner == msg.sender, "owner is not as same as msg.sender");
        uint new_assetAmount = assetAmount + mintAmount;
        
        Asset storage asset = _assetMap[assetToken];
        (int relativeAssetPrice, ) = IPriceFeed(asset.priceFeed).getLatestPrice();
        uint8 decimal = sAsset(assetToken).decimals();
        uint new_ratio = cur_collateralAmount * (10 ** decimal) / new_assetAmount / uint(relativeAssetPrice);
        require(new_ratio >= _assetMap[assetToken].minCollateralRatio, "new ratio is below MRC");
        
        sAsset(assetToken).mint(owner, mintAmount);
        // update position
        _idxPositionMap[positionIndex].assetAmount = new_assetAmount;
    }

    // Contract burns the given amount of asset tokens in the position. Make sure the message sender owns the position.
    function burn(uint positionIndex, uint burnAmount) override external {
        require(positionIndex >= 0, "invalid positionIndex");
        (address owner, uint cur_collateralAmount, address assetToken, uint assetAmount) = this.getPosition(positionIndex);
        require(owner == msg.sender, "owner is not as same as msg.sender");
        int new_assetAmount = int(assetAmount) - int(burnAmount);
        require(new_assetAmount >= 0, "new_assetAmount < 0");
        sAsset(assetToken).burn(address(msg.sender), burnAmount);
        _idxPositionMap[positionIndex].assetAmount = uint(new_assetAmount);
    }
}