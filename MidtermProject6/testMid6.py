
# Python program to demonstrate
# Conversion of JSON data to
# dictionary
 
 
# importing the module
import json

vec0 = []
vec1 = []
vec2 = []
for i in range(3):
    # Opening JSON file
    filename = "700" + str(i) + "_state.json"
    with open(filename) as json_file:
        data = json.load(json_file)
        print("Processing", filename)
        for account_info in data:
            if i == 0:
                vec0.append(account_info)
            elif i == 1:
                vec1.append(account_info)
            else:
                vec2.append(account_info)
vec0.sort()
vec1.sort()
vec2.sort()

if vec0 == vec1 and vec1 == vec2:
    print("Success!")
else:
    if vec0 != vec1:
        print("7000 != 7001")
    if vec1 != vec2:
        print("7001 != 7002")
    if vec2 != vec0:
        print("7002 != 7000")


