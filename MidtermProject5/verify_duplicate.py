
# Python program to demonstrate
# Conversion of JSON data to
# dictionary
 
 
# importing the module
import json
for i in range(3):
    # Opening JSON file
    filename = "700" + str(i) + "-tx.json"
    with open(filename) as json_file:
        data = json.load(json_file)

        print("Filename:", filename)
        myDict = {}
        num_blk = -1
        for txns_per_blk in data:
            num_blk += 1
            for txn in txns_per_blk:
                if txn in myDict.keys():
                    myDict[txn] += 1
                else:
                    myDict[txn] = 1
        total_num = 0
        unique_num = 0
        for key, value in myDict.items():
            total_num += value
            if value == 1:
                unique_num += value
        print("Transaction throughput: {:.2f} (per min)".format( float(total_num / 5)) )
        print("Transactions per block: {:.2f}".format( float(total_num / num_blk)) )
        print("Unique rate: {:.4f}".format( float( unique_num / total_num)) )
        print("First txn of second blk:", data[1][0])
        print("-"*20)


