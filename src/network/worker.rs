use super::message::Message;
use super::peer;
use super::server::Handle as ServerHandle;
use crate::types::hash::H256;
use crate::blockchain::Blockchain;
use std::sync::{Arc, Mutex};
use log::{debug, warn, error};
use crate::types::hash::Hashable;
use std::collections::HashMap;
use crate::types::block::Block;
use std::thread;
use ring::digest;
use ring::signature::{Ed25519KeyPair, Signature, KeyPair, VerificationAlgorithm, EdDSAParameters, UnparsedPublicKey, ED25519};
use crate::types::transaction::{Transaction, SignedTransaction, MemPool};
use crate::types::state::{State, StatePerBlock};
use crate::types::address::{Address};

#[cfg(any(test,test_utilities))]
use super::peer::TestReceiver as PeerTestReceiver;
#[cfg(any(test,test_utilities))]
use super::server::TestReceiver as ServerTestReceiver;
#[derive(Clone)]
pub struct Worker {
    msg_chan: smol::channel::Receiver<(Vec<u8>, peer::Handle)>,
    num_worker: usize,
    server: ServerHandle,
    blockchain: Arc<Mutex<Blockchain>>,
    orphan_buf: Arc<Mutex<HashMap<H256, Block>>>,
    mempool: Arc<Mutex<MemPool>>,
    state: Arc<Mutex<State>>,
    state_per_blk: Arc<Mutex<StatePerBlock>>,
}


impl Worker {
    pub fn new(
        num_worker: usize,
        msg_src: smol::channel::Receiver<(Vec<u8>, peer::Handle)>,
        server: &ServerHandle,
        blockchain: &Arc<Mutex<Blockchain>>,
        orphan_buf: &Arc<Mutex<HashMap<H256, Block>>>,
        mempool: &Arc<Mutex<MemPool>>,
        state: &Arc<Mutex<State>>,
        state_per_blk: &Arc<Mutex<StatePerBlock>>,
    ) -> Self {
        Self {
            msg_chan: msg_src,
            num_worker,
            server: server.clone(),
            blockchain: Arc::clone(blockchain),
            orphan_buf: Arc::clone(orphan_buf),
            mempool: Arc::clone(mempool),
            state: Arc::clone(state),
            state_per_blk: Arc::clone(state_per_blk),
        }
    }

    pub fn start(self) {
        let num_worker = self.num_worker;
        for i in 0..num_worker {
            let mut cloned = self.clone();
            thread::spawn(move || {
                cloned.worker_loop();
                warn!("Worker thread {} exited", i);
            });
        }
    }

    fn worker_loop(&mut self) {
        let mut num_blks = 0;
        loop {
            let result = smol::block_on(self.msg_chan.recv());
            if let Err(e) = result {
                error!("network worker terminated {}", e);
                break;
            }
            let msg = result.unwrap();
            let (msg, mut peer) = msg;
            let msg: Message = bincode::deserialize(&msg).unwrap();
            match msg {
                Message::Ping(nonce) => {
                    debug!("Ping: {}", nonce);
                    peer.write(Message::Pong(nonce.to_string()));
                }
                Message::Pong(nonce) => {
                    debug!("Pong: {}", nonce);
                }
                // _ => unimplemented!(),
                Message::NewBlockHashes(blk_hashes) => {
                    // debug!("NewBlockHashes");
                    println!("NewBlockHashes");
                    // orange block receives NewBlockHashes msg
                    let mut not_in_blkchain = Vec::new();
                    let chain_unwrap = self.blockchain.lock().unwrap();
                    for hash in blk_hashes.clone(){
                        if chain_unwrap.blk_map.contains_key(&hash) == false {
                            not_in_blkchain.push(hash);
                        }
                    }
                    peer.write(Message::GetBlocks(not_in_blkchain)); // write msg to blue
                }
                Message::GetBlocks(blk_hashes) => {
                    // debug!("GetBlocks");
                    println!("GetBlocks");
                    // blue receive GetBlocks (hashes are those missed blks in orange)
                    let mut valid_blks = Vec::new();
                    let chain_unwrap = self.blockchain.lock().unwrap();
                    for hash in blk_hashes { 
                        if chain_unwrap.blk_map.contains_key(&hash) == true {
                            let block = chain_unwrap.blk_map[&hash].clone();
                            valid_blks.push(block);
                        }
                    }
                    peer.write(Message::Blocks(valid_blks)); // get blocks and ask orange to insert these blocks
                }
                Message::Blocks(valid_blks) => {
                    // debug!("Blocks");
                    println!("Blocks");
                    let mut chain_unwrap = self.blockchain.lock().unwrap();
                    let mut new_blk_hashes = Vec::new();
                    let mut parent_check_fail_hashes = Vec::new();
                    let mut state_unwrap = self.state.lock().unwrap();

                    for block in valid_blks {
                        // check txns in block

                        // num_blks += 1;
                        // if num_blks % 50 == 0 {
                        //     println!("{:?} blocks received.", num_blks);
                        // }
                        let mut orphan_buffer = self.orphan_buf.lock().unwrap();
                        if !chain_unwrap.blk_map.contains_key(&block.hash()) {
                            if chain_unwrap.blk_map.contains_key(&block.header.parent) == false {
                                // Parent check: orphan
                                let mut tmp_blk = block.clone();
                                parent_check_fail_hashes.push(block.header.parent);
                                orphan_buffer.insert(block.header.parent, tmp_blk);
                                
                                // todo:
                                // If this check fails, also send GetBlocks message, containing this parent hash. (This is the same as part 3 instructs.
                                
                            }
                            else if block.hash() <= block.header.difficulty && block.header.difficulty == chain_unwrap.blk_map[&block.header.parent].header.difficulty{
                                // Parent check: not orphan -> PoW validity check
                                chain_unwrap.insert(&block);
                                new_blk_hashes.push(block.hash());

                                // Get a block to be inserted, which means the block content should not be in mempool
                                // since removal of txns is not broadcasted, we have to maintain it locally
                                let vec_txns = block.clone().content.data;
                                let mut mempool_unwrap = self.mempool.lock().unwrap();
                                for txn in vec_txns {
                                    mempool_unwrap.remove(&txn);
                                    state_unwrap.update(&txn);
                                }
                                let mut cur_tip = block.clone();
                                // let mut cur_tip = block;
                                while true {
                                    // block in oprhan buffer that has current block hash as parent
                                    if orphan_buffer.contains_key(&cur_tip.hash()){
                                        let blk = orphan_buffer.remove(&cur_tip.hash()).unwrap();

                                        // locally remove its content (vec of sig txns) from mempool
                                        let vec_txns_orphan = blk.clone().content.data;
                                        for txn in vec_txns_orphan {
                                            mempool_unwrap.remove(&txn);
                                            state_unwrap.update(&txn);
                                        }
                                        // insert blk into blockchain
                                        chain_unwrap.insert(&blk);
                                        new_blk_hashes.push(blk.hash());
                                        cur_tip = blk;
                                    }
                                    else { 
                                        break; 
                                    }
                                }
                            }
                        }
                        // update state_per_blk with new block hash and new state
                        // let block = chain_unwrap.blk_map[&hash].clone();
                        let mut state_per_blk_unwrap = self.state_per_blk.lock().unwrap();
                        state_per_blk_unwrap.blkStateMap.insert(block.hash(), state_unwrap.clone());
                        //state_per_blk_unwrap.insertState(block.hash(), state_unwrap.ledger.clone());
                        drop(state_per_blk_unwrap);
                    }
                    if parent_check_fail_hashes.len() != 0 {
                        self.server.broadcast(Message::GetBlocks(parent_check_fail_hashes));
                    }
                    if new_blk_hashes.len() != 0 {
                        self.server.broadcast(Message::NewBlockHashes(new_blk_hashes));
                    }
                }
                Message::NewTransactionHashes(txn_hashes) => {
                    println!("NewTransactionHashes");
                    let mut not_in_mempool = Vec::new();
                    let mempool_unwrap = self.mempool.lock().unwrap();
                    for hash in txn_hashes { 
                        // CHECK LATER
                        if mempool_unwrap.txn_set.contains(&hash) == false {
                            not_in_mempool.push(hash);
                        }
                        // if mempool_unwrap.txn_map.contains_key(&hash) == false {
                        //     not_in_mempool.push(hash);
                        // }
                    }
                    peer.write(Message::GetTransactions(not_in_mempool)); // write msg to blue
                }
                Message::GetTransactions(txn_hashes) => {
                    println!("GetTransactions");
                    // blue receive GetBlocks (hashes are those missed blks in orange)
                    let mut valid_txns = Vec::new();
                    let mempool_unwrap = self.mempool.lock().unwrap();
                    for hash in txn_hashes { 
                        if mempool_unwrap.txn_map.contains_key(&hash) == true {
                            let txn = mempool_unwrap.txn_map[&hash].clone();
                            valid_txns.push(txn);
                        }
                    }
                    peer.write(Message::Transactions(valid_txns)); // get blocks and ask orange to insert these blocks
                }
                Message::Transactions(valid_txns) => {
                    println!("Transactions");
                    let tip = self.blockchain.lock().unwrap().tip();
                    let mut state_per_blk_unwrap = self.state_per_blk.lock().unwrap(); // use weird hashmap to get the state of the blkchain tip
                    println!("{:?}", tip);
                    let mut state_unwrap = &state_per_blk_unwrap.blkStateMap[&tip];
                    drop(tip);
                    let mut mempool_unwrap = self.mempool.lock().unwrap();
                    for txn in valid_txns{
                        let _txn = txn.clone().transaction;
                        let _sig = txn.clone().signature;
                        let _pkey = txn.clone().public_key;
                        let tmp = bincode::serialize(&_txn).unwrap();
                        // Check if the transaction is signed correctly by the public key(s).
                        let txn_id = digest::digest(&digest::SHA256, digest::digest(&digest::SHA256, tmp.as_ref()).as_ref());
                        let public_key = UnparsedPublicKey::new(&ED25519, _pkey.clone());
                        let verifed = public_key.verify(txn_id.as_ref(), &_sig).is_ok();
                        let mut hash: H256 = txn.hash();
                        if verifed {
                            // Check if public key matches the address of the withdrawing account owner (sender)
                            let sen_addr = Address::from_public_key_bytes(&_pkey);
                            if state_unwrap.ledger.contains_key(&sen_addr){
                                // Spending Check:  balance is enough && nonce == nonce + 1
                                if state_unwrap.ledger[&sen_addr].1 >= _txn.value 
                                && _txn.nonce == state_unwrap.ledger[&sen_addr].0 + 1 {
                                    self.server.broadcast(Message::NewTransactionHashes(vec![hash]));
                                    mempool_unwrap.insert(&txn);
                                } else {
                                    println!("Msg::Txns => Spending & Nonce check failed");
                                }
                            } else {
                                println!("Msg::Txns => No sender exists in the ledger");
                            }
                        } else {
                            println!("Msg::Txns => Public key signature check failed");
                        }
                    }
                }
            }
        }
    }
}

#[cfg(any(test,test_utilities))]
struct TestMsgSender {
    s: smol::channel::Sender<(Vec<u8>, peer::Handle)>
}
#[cfg(any(test,test_utilities))]
impl TestMsgSender {
    fn new() -> (TestMsgSender, smol::channel::Receiver<(Vec<u8>, peer::Handle)>) {
        let (s,r) = smol::channel::unbounded();
        (TestMsgSender {s}, r)
    }

    fn send(&self, msg: Message) -> PeerTestReceiver {
        let bytes = bincode::serialize(&msg).unwrap();
        let (handle, r) = peer::Handle::test_handle();
        smol::block_on(self.s.send((bytes, handle))).unwrap();
        r
    }
}
#[cfg(any(test,test_utilities))]
/// returns two structs used by tests, and an ordered vector of hashes of all blocks in the blockchain
fn generate_test_worker_and_start() -> (TestMsgSender, ServerTestReceiver, Vec<H256>) {
    let (server, server_receiver) = ServerHandle::new_for_test();
    let (test_msg_sender, msg_chan) = TestMsgSender::new();
    let blockchain = Blockchain::new();
    let vec_of_hashes = blockchain.all_blocks_in_longest_chain();
    let blockchain = Arc::new(Mutex::new(blockchain));
    let worker = Worker::new(1, msg_chan, &server, &blockchain);
    worker.start(); 
    (test_msg_sender, server_receiver, vec_of_hashes)
}

// DO NOT CHANGE THIS COMMENT, IT IS FOR AUTOGRADER. BEFORE TEST


#[cfg(test)]
mod test {
    use ntest::timeout;
    use crate::types::block::generate_random_block;
    use crate::types::hash::Hashable;

    use super::super::message::Message;
    use super::generate_test_worker_and_start;

    #[test]
    #[timeout(60000)]
    fn sp2022autograder041() {
        let (test_msg_sender, _server_receiver, v) = generate_test_worker_and_start();
        let random_block = generate_random_block(v.last().unwrap());
        let mut peer_receiver = test_msg_sender.send(Message::NewBlockHashes(vec![random_block.hash()]));
        let reply = peer_receiver.recv();
        if let Message::GetBlocks(v) = reply {
            assert_eq!(v, vec![random_block.hash()]);
        } else {
            panic!();
        }
    }
    #[test]
    #[timeout(60000)]
    fn sp2022autograder042() {
        let (test_msg_sender, _server_receiver, v) = generate_test_worker_and_start();
        let h = v.last().unwrap().clone();
        let mut peer_receiver = test_msg_sender.send(Message::GetBlocks(vec![h.clone()]));
        let reply = peer_receiver.recv();
        if let Message::Blocks(v) = reply {
            assert_eq!(1, v.len());
            assert_eq!(h, v[0].hash())
        } else {
            panic!();
        }
    }
    #[test]
    #[timeout(60000)]
    fn sp2022autograder043() {
        let (test_msg_sender, server_receiver, v) = generate_test_worker_and_start();
        let random_block = generate_random_block(v.last().unwrap());
        let mut _peer_receiver = test_msg_sender.send(Message::Blocks(vec![random_block.clone()]));
        let reply = server_receiver.recv().unwrap();
        if let Message::NewBlockHashes(v) = reply {
            assert_eq!(v, vec![random_block.hash()]);
        } else {
            panic!();
        }
    }
}

// DO NOT CHANGE THIS COMMENT, IT IS FOR AUTOGRADER. AFTER TEST
