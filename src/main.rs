#[cfg(test)]
#[macro_use]
extern crate hex_literal;

pub mod api;
pub mod blockchain;
pub mod types;
pub mod miner;
pub mod network;
pub mod tx_generator;

use blockchain::Blockchain;
use clap::clap_app;
use smol::channel;
use log::{error, info};
use api::Server as ApiServer;
use std::net;
use std::process;
use std::sync::{Arc, Mutex};
use std::thread;
use std::time;
use std::collections::HashMap;
use crate::types::transaction::MemPool;
use crate::types::state::{State, StatePerBlock};
use crate::tx_generator::keypair_lookup::{KeypairMap};
use crate::types::address::{Address};

fn main() {
    // parse command line arguments
    let matches = clap_app!(Bitcoin =>
     (version: "0.1")
     (about: "Bitcoin client")
     (@arg verbose: -v ... "Increases the verbosity of logging")
     (@arg peer_addr: --p2p [ADDR] default_value("127.0.0.1:6000") "Sets the IP address and the port of the P2P server")
     (@arg api_addr: --api [ADDR] default_value("127.0.0.1:7000") "Sets the IP address and the port of the API server")
     (@arg known_peer: -c --connect ... [PEER] "Sets the peers to connect to at start")
     (@arg p2p_workers: --("p2p-workers") [INT] default_value("4") "Sets the number of worker threads for P2P server")
    )
    .get_matches();

    // init logger
    let verbosity = matches.occurrences_of("verbose") as usize;
    stderrlog::new().verbosity(verbosity).init().unwrap();
    let blockchain = Blockchain::new();
    let blockchain = Arc::new(Mutex::new(blockchain));
    let buffer = HashMap::new();
    let buffer = Arc::new(Mutex::new(buffer));
    let mempool = MemPool::new();
    let mempool = Arc::new(Mutex::new(mempool));
    let keypair_map = KeypairMap::new();
    let init_addr = Address([0u8; 20]);
    let state = Arc::new(Mutex::new(State::new(init_addr)));
    // let state_per_blk: Arc<Mutex<StatePerBlock>> = Arc::new(Mutex::new(StatePerBlock(HashMap::new())));
    let tip = blockchain.lock().unwrap().tip();
    let state_per_blk = Arc::new(Mutex::new(StatePerBlock::new(tip)));
    drop(tip);
    
    // parse p2p server address
    let p2p_addr = matches
        .value_of("peer_addr")
        .unwrap()
        .parse::<net::SocketAddr>()
        .unwrap_or_else(|e| {
            error!("Error parsing P2P server address: {}", e);
            process::exit(1);
        });
    let peer_address = p2p_addr.to_string();
    println!("{:?}", peer_address);
    // parse api server address
    let api_addr = matches
        .value_of("api_addr")
        .unwrap()
        .parse::<net::SocketAddr>()
        .unwrap_or_else(|e| {
            error!("Error parsing API server address: {}", e);
            process::exit(1);
        });

    // create channels between server and worker
    let (msg_tx, msg_rx) = channel::bounded(10000);

    // start the p2p server
    let (server_ctx, server) = network::server::new(p2p_addr, msg_tx).unwrap();
    server_ctx.start().unwrap();

    // start the worker
    let p2p_workers = matches
        .value_of("p2p_workers")
        .unwrap()
        .parse::<usize>()
        .unwrap_or_else(|e| {
            error!("Error parsing P2P workers: {}", e);
            process::exit(1);
        });
    let worker_ctx = network::worker::Worker::new(
        p2p_workers,
        msg_rx,
        &server,
        &blockchain,
        &buffer,
        &mempool,
        &state,
        &state_per_blk,
    );
    worker_ctx.start();

    // start the tx-generator
    let (txgen_ctx, txgen) = tx_generator::new(&mempool, &server, keypair_map, &state_per_blk, peer_address, &blockchain);
    txgen_ctx.start();
  
    // start the miner
    let (miner_ctx, miner, finished_block_chan) = miner::new(&blockchain, &mempool, &state_per_blk, &state);
    let miner_worker_ctx = miner::worker::Worker::new(&server, finished_block_chan, &blockchain);
    miner_ctx.start();
    miner_worker_ctx.start();

    // connect to known peers
    if let Some(known_peers) = matches.values_of("known_peer") {
        let known_peers: Vec<String> = known_peers.map(|x| x.to_owned()).collect();
        let server = server.clone();
        thread::spawn(move || {
            for peer in known_peers {
                loop {
                    let addr = match peer.parse::<net::SocketAddr>() {
                        Ok(x) => x,
                        Err(e) => {
                            error!("Error parsing peer address {}: {}", &peer, e);
                            break;
                        }
                    };
                    match server.connect(addr) {
                        Ok(_) => {
                            info!("Connected to outgoing peer {}", &addr);
                            break;
                        }
                        Err(e) => {
                            error!(
                                "Error connecting to peer {}, retrying in one second: {}",
                                addr, e
                            );
                            thread::sleep(time::Duration::from_millis(1000));
                            continue;
                        }
                    }
                }
            }
        });
    }


    // start the API server
    ApiServer::start(
        api_addr,
        &miner,
        &server,
        &blockchain,
        &txgen,
        &state_per_blk,
    );

    loop {
        std::thread::park();
    }
}