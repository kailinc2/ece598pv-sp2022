use serde::{Serialize,Deserialize};
use std::collections::{HashMap};
use crate::types::transaction::{Transaction, SignedTransaction, MemPool};
use ring::signature::{Ed25519KeyPair, Signature, KeyPair, VerificationAlgorithm, EdDSAParameters, UnparsedPublicKey, ED25519};
use crate::types::address::Address;
use crate::types::key_pair;
use crate::types::hash::{H256, Hashable};
use crate::types::block::{Block, Header, Content};
use crate::types::merkle::MerkleTree;

#[derive(Serialize, Deserialize, Debug, Default, Clone)]
pub struct State {
    pub ledger: HashMap<Address, (u128, i32)>, // account address, (nonce, balance)
}

impl State{
    pub fn new(init_addr: Address) -> Self {
        let init_nonce = 0;
        let init_balance = 100;
        let mut acc_info = HashMap::new();
        acc_info.insert(init_addr, (init_nonce, init_balance));
        println!("ICO => \'{:?}\' has {:?} coins", init_addr, init_balance);
        State{ledger: acc_info}
    }
    pub fn newFromLedger (ledger: HashMap<Address, (u128, i32)>) -> Self {
        State{ledger: ledger}
    }
    pub fn update(&mut self, sig_txn: &SignedTransaction){
        
        let txn = sig_txn.transaction.clone();
        let sender = txn.sender;
        let receiver = txn.receiver;
        let value = txn.value;
        let nonce = txn.nonce;
        // check new_nonce == old_nonce + 1
        if nonce == self.ledger[&sender].0 + 1 {
            // check if sender has enough money
            if self.ledger[&sender].1 >= value {
                println!("sender has enough money.");
                // Sender 
                let new_balance_sen = self.ledger[&sender].1 - value;
                let new_nonce_sen = nonce;
                *self.ledger.get_mut(&sender).unwrap() = (new_nonce_sen, new_balance_sen);
                // Receiver (without nonce update)
                if self.ledger.contains_key(&receiver) {
                    let new_balance_rec = self.ledger[&receiver].1 + value;
                    let org_nonce_rec = self.ledger[&receiver].0;
                    *self.ledger.get_mut(&receiver).unwrap() = (org_nonce_rec, new_balance_rec);
                }
                else{
                    self.ledger.insert(receiver, (0, value));
                }
            }
        }
    }

}

#[derive(Serialize, Deserialize, Debug, Default, Clone)]
pub struct StatePerBlock {
    pub blkStateMap: HashMap<H256, State>, // blkhash, State
}
// pub struct StatePerBlock(pub HashMap<H256, State>);

impl StatePerBlock {
    pub fn new(genesis_hash: H256) -> Self {
        let mut init_blkStateMap = HashMap::new();

        // hardcode genesis block hash and insert to init_blkStateMap 
        // let parent: H256 = [0u8; 32].into(); // parent for genesis block
        // let nonce: u32 = 0;
        // let mut tmp = [128u8; 32];
        // tmp[0] = 0;
        // // tmp[1] = 0;
        // // tmp[2] = 128u8;
        // let difficulty: H256 = tmp.into();
        // let timestamp = 0;
        // let transaction = Vec::new();
        // let tree = MerkleTree::new(&transaction);
        // let merkle_root = tree.root();
        // let header = Header{parent: parent, nonce: nonce, difficulty: difficulty, 
        //                     timestamp: timestamp, merkle_root: merkle_root};
        // let content = Content{data: transaction};
        // let genesis = Block{header: header, content: content};
        // let genesis_hash: H256 = genesis.hash();
        println!("genesis hash : {:?}", genesis_hash);
        let init_addr = Address([0u8; 20]);
        let genesis_state = State::new(init_addr);
        init_blkStateMap.insert(genesis_hash, genesis_state);
        StatePerBlock{blkStateMap: init_blkStateMap}
    }
    // pub fn insertState(&mut self, blockhash: H256, new_state: HashMap<Address, (u128, i32)>) {
    //     let state_to_insert = State::newFromLedger(new_state.clone());
    //     self.blkStateMap.insert(blockhash, state_to_insert);
    // }
    // pub fn get(&mut self, blockhash: &H256) -> State {
    //     self.blkStateMap.get(&blockhash)
    // }
}