use serde::{Serialize,Deserialize};
use ring::signature::{Ed25519KeyPair, Signature, KeyPair, VerificationAlgorithm, EdDSAParameters, UnparsedPublicKey, ED25519};
use rand::Rng;
use crate::types::address::Address;
use crate::types::hash::{H256, Hashable};
use std::collections::{HashMap, HashSet};

#[derive(Serialize, Deserialize, Debug, Default, Clone)]
// nonce: nonce for sender
pub struct Transaction {
    pub sender: Address,
    pub receiver: Address,
    pub value: i32,
    pub nonce: u128, 
}

#[derive(Serialize, Deserialize, Debug, Default, Clone)]
// public_key: sender's public key
pub struct SignedTransaction {
    pub transaction: Transaction,
    pub signature: Vec<u8>,
    pub public_key: Vec<u8>,
}

impl Hashable for SignedTransaction {
    fn hash(&self) -> H256 {
        let s = bincode::serialize(&self).unwrap();
        let hashed : H256 = ring::digest::digest(&ring::digest::SHA256, s.as_ref()).into();
        hashed
    }
}

pub struct MemPool {
    pub txn_map: HashMap<H256, SignedTransaction>,
    pub txn_set: HashSet<H256>,
}

impl MemPool {
    pub fn new() -> Self {
        let mut txn_map = HashMap::new();
        let mut txn_set = HashSet::new();
        MemPool {txn_map: txn_map, txn_set: txn_set}
    }
    
    pub fn insert(&mut self, txn: &SignedTransaction) {
        let txn_hash: H256 = txn.hash();
        self.txn_map.insert(txn_hash, txn.clone());
        self.txn_set.insert(txn_hash);
    }

    pub fn remove(&mut self, txn: &SignedTransaction) {
        let txn_hash: H256 = txn.hash();
        if self.txn_map.contains_key(&txn_hash) {
            self.txn_map.remove(&txn_hash);
        }
    }

}
/// Create digital signature of a transaction
pub fn sign(t: &Transaction, key: &Ed25519KeyPair) -> Signature {
    let encoded: Vec<u8> = bincode::serialize(&t).unwrap();
    let signature = key.sign(&encoded[..]); 
    signature
}

/// Verify digital signature of a transaction, using public key instead of secret key
pub fn verify(t: &Transaction, public_key: &[u8], signature: &[u8]) -> bool {
    let original_transaction: Vec<u8> = bincode::serialize(&t).unwrap();
    let other_public_key = UnparsedPublicKey::new(&ED25519, public_key);
    other_public_key.verify(&original_transaction[..], signature).is_ok()
}

#[cfg(any(test, test_utilities))]
pub fn generate_random_transaction() -> Transaction {
    let mut rng = rand::thread_rng();
    let rand_sender = Address(rng.gen::<[u8; 20]>());
    let rand_receiver = Address(rng.gen::<[u8; 20]>());
    let rand_value = rng.gen::<i32>();
    // println!("{:?}", rand_sender);
    // println!("{:?}", rand_receiver);
    // println!("{:?}", rand_value);
    Transaction{sender: rand_sender, receiver: rand_receiver, value: rand_value}
}

// DO NOT CHANGE THIS COMMENT, IT IS FOR AUTOGRADER. BEFORE TEST

#[cfg(test)]
mod tests {
    use super::*;
    use crate::types::key_pair;
    use ring::signature::KeyPair;


    #[test]
    fn sign_verify() {
        let t = generate_random_transaction();
        let key = key_pair::random();
        let signature = sign(&t, &key);
        assert!(verify(&t, key.public_key().as_ref(), signature.as_ref()));
    }
    #[test]
    fn sign_verify_two() {
        let t = generate_random_transaction();
        let key = key_pair::random();
        let signature = sign(&t, &key);
        let key_2 = key_pair::random();
        let t_2 = generate_random_transaction();
        assert!(!verify(&t_2, key.public_key().as_ref(), signature.as_ref()));
        assert!(!verify(&t, key_2.public_key().as_ref(), signature.as_ref()));
    }
}

// DO NOT CHANGE THIS COMMENT, IT IS FOR AUTOGRADER. AFTER TEST