use super::hash::{Hashable, H256};
use std::collections::VecDeque;

/// A Merkle tree.
#[derive(Debug, Default)]
pub struct MerkleTree {
    nodes: Vec<H256>,
    cnt_leaves: usize,
}

impl MerkleTree {
    pub fn new<T>(data: &[T]) -> Self where T: Hashable, {
        let mut q: VecDeque<H256> = VecDeque::new();
        let mut m_tree: Vec<H256> = Vec::new();
        let mut data_len = data.len();

        for node in data.iter(){ // iterate by value
            q.push_back(node.hash());
        }
        // check even or odd, pad with last node if odd
        if data_len % 2 != 0 {
            q.push_back(data[data_len-1].hash());
            data_len += 1;
        }

        let leaves_len = data_len;
        let mut lvl_cnt = 0;
        let mut lvl_len = leaves_len;
        while !q.is_empty() {
            let node1 = q.pop_front().unwrap();
            m_tree.push(node1);
            lvl_cnt += 1;
            let tmp = q.pop_front();
            if tmp == None {
                // node 1 is root
                break;
            }
            let node2 = tmp.unwrap();
            m_tree.push(node2);
            lvl_cnt += 1;
            let left_hash = <[u8;32]>::from(node1);
            let right_hash = <[u8;32]>::from(node2);
            let concat_hash = [&left_hash[..], &right_hash[..]].concat();
            let parent_node = ring::digest::digest(&ring::digest::SHA256, &concat_hash[..]).into();
            q.push_back(parent_node);
            if lvl_cnt == lvl_len && lvl_cnt != 2 { // end of the layer and not the second last layer
                let i = lvl_len/2;
                if i % 2 != 0 { // pad a node if odd
                    q.push_back(parent_node);
                    lvl_len = i + 1; // update len for next level
                    lvl_cnt = 0;
                } else {
                    lvl_len = i;
                    lvl_cnt = 0;
                }
            }
        }
        MerkleTree{nodes: m_tree, cnt_leaves: leaves_len}
    }

    pub fn root(&self) -> H256 {
        if self.nodes.len() == 0 {
            let tmp: H256 = [0u8; 32].into();
            return tmp;
        }
        self.nodes[self.nodes.len()-1]
    }

    /// Returns the Merkle Proof of data at index i
    pub fn proof(&self, index: usize) -> Vec<H256> {
        let mut proof:Vec<H256> = Vec::new(); // vector of required siblings hashes
        let mut i = index; // initialize i at index
        let mut offset = 0;
        let mut group_num = index / 2;
    
        let mut size = self.cnt_leaves; 
        if size % 2 != 0 {
            size += 1;
        }

        while size != 1 {
            if i % 2 == 1 {
                proof.push(self.nodes[i - 1]); // left sibling 
            }
            else {
                proof.push(self.nodes[i + 1]) // right sibling
            }

            offset += size;
            i = offset + group_num; // calculate indices of the path
            group_num /= 2;
            size /= 2; // move to upper level
            if size % 2 != 0 && size != 1 {
                size += 1;
            } 
        }
        proof
    }
}

/// Verify that the datum hash with a vector of proofs will produce the Merkle root. Also need the
/// index of datum and `leaf_size`, the total number of leaves.
pub fn verify(root: &H256, datum: &H256, proof: &[H256], index: usize, leaf_size: usize) -> bool {
    let mut cur_hash = <[u8;32]>::from(datum); // cast H256 to [u8:32]
    let mut idx = index;

    for i in 0..proof.len() {
        let other_hash = <[u8;32]>::from(proof[i]);
        if idx % 2 == 0 { 
            // current hash is left node
            let concat_hash = [&cur_hash[..], &other_hash[..]].concat();
            cur_hash[0..32].copy_from_slice(ring::digest::digest(&ring::digest::SHA256, &concat_hash[..]).as_ref());

        } else {
            // current hash is right node
            let concat_hash = [&other_hash[..], &cur_hash[..]].concat();
            cur_hash[0..32].copy_from_slice(ring::digest::digest(&ring::digest::SHA256, &concat_hash[..]).as_ref());
        }
        idx = idx / 2;
    }
    if cur_hash == <[u8;32]>::from(root) {
        return true;
    } 
    false
}
// DO NOT CHANGE THIS COMMENT, IT IS FOR AUTOGRADER. BEFORE TEST

#[cfg(test)]
mod tests {
    use crate::types::hash::H256;
    use super::*;

    macro_rules! gen_merkle_tree_data {
        () => {{
            vec![
                (hex!("0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d")).into(),
                (hex!("0101010101010101010101010101010101010101010101010101010101010202")).into(),
            ]
        }};
    }

    #[test]
    fn merkle_root() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let root = merkle_tree.root();
        assert_eq!(
            root,
            (hex!("6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920")).into()
        );
        // "b69566be6e1720872f73651d1851a0eae0060a132cf0f64a0ffaea248de6cba0" is the hash of
        // "0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d"
        // "965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f" is the hash of
        // "0101010101010101010101010101010101010101010101010101010101010202"
        // "6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920" is the hash of
        // the concatenation of these two hashes "b69..." and "965..."
        // notice that the order of these two matters
    }

    #[test]
    fn merkle_proof() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(0);
        assert_eq!(proof,
                   vec![hex!("965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f").into()]
        );
        // "965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f" is the hash of
        // "0101010101010101010101010101010101010101010101010101010101010202"
    }

    #[test]
    fn merkle_verifying() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(0);
        assert!(verify(&merkle_tree.root(), &input_data[0].hash(), &proof, 0, input_data.len()));
    }
}

// DO NOT CHANGE THIS COMMENT, IT IS FOR AUTOGRADER. AFTER TEST