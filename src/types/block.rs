use serde::{Serialize, Deserialize};
use crate::types::hash::{H256, Hashable};
use crate::types::transaction::{SignedTransaction};
use crate::types::merkle::MerkleTree;
use rand::Rng;
use std::time;
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Header {
    pub parent: H256,
	pub nonce: u32,
	pub difficulty: H256,
	pub timestamp: u128, 
	pub merkle_root: H256,
}

impl Hashable for Header {
    fn hash(&self) -> H256 {
        //unimplemented!()
        let header_bin = bincode::serialize(&self).unwrap();
        let hashed: H256 = ring::digest::digest(&ring::digest::SHA256, header_bin.as_ref()).into();
        hashed
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Content {
	pub data: Vec<SignedTransaction>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Block {
    pub header: Header,
	pub content: Content,
	
}

impl Hashable for Block {
    fn hash(&self) -> H256 {
        self.header.hash()
    }
}

impl Block {
    pub fn get_parent(&self) -> H256 {
        //unimplemented!()
        self.header.parent
    }

    pub fn get_difficulty(&self) -> H256 {
        //unimplemented!()
        self.header.difficulty
    }
}

#[cfg(any(test, test_utilities))]
pub fn generate_random_block(parent: &H256) -> Block {
    //unimplemented!()
    let mut rng = rand::thread_rng();
    let nonce: u32 = rng.gen();
    let mut tmp = [255u8; 32];
    tmp[0] = 0;
    let difficulty: H256 = tmp.into();
    let timestamp: u128 = time::SystemTime::now().duration_since(time::UNIX_EPOCH).expect("Wrong order").as_millis();
    let transaction = Vec::new();
    let tree = MerkleTree::new(&transaction);
    let merkle_root = tree.root();

    let header = Header{ parent: *parent, nonce: nonce, difficulty: difficulty, timestamp: timestamp, merkle_root: merkle_root };
    let content = Content{ data: transaction };
    Block{ header: header, content: content }
}