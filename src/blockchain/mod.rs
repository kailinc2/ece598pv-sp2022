use crate::types::block::{Block, Header, Content};
use crate::types::hash::{H256, Hashable};
use crate::types::merkle::MerkleTree;
use std::collections::{VecDeque, HashMap};
use crate::types::state::{State, StatePerBlock};
use std::sync::{Arc, Mutex};
use crate::Address;
// use crate::types::transaction::{Transaction, SignedTransaction};
// use std::time;

pub struct Blockchain {
    pub blk_map: HashMap<H256, Block>,
    pub len_map: HashMap<H256, usize>,
    tip: H256,
}

impl Blockchain {
    /// Create a new blockchain, only containing the genesis block
    pub fn new() -> Self {
        // generate genesis block
        let parent: H256 = [0u8; 32].into(); // parent for genesis block
        let nonce: u32 = 0;
        // let difficulty: H256 = [255u8; 32].into(); // 0xffff...fff
        // let difficulty: H256 = [1u8; 32].into();
        let mut tmp = [64u8; 32]; // 64: panicked at tx-generator, 128 is working
        tmp[0] = 0;
        tmp[1] = 8;
        // tmp[2] = 128u8;
        let difficulty: H256 = tmp.into();
        // let timestamp: u128 = time::SystemTime::now().duration_since(time::UNIX_EPOCH).expect("Wrong order").as_millis();
        let timestamp = 0;
        let transaction = Vec::new();
        let tree = MerkleTree::new(&transaction);
        let merkle_root = tree.root();
        let header = Header{parent: parent, nonce: nonce, difficulty: difficulty, 
                            timestamp: timestamp, merkle_root: merkle_root};
        let content = Content{data: transaction};
        let genesis = Block{header: header, content: content};
        let mut blk_map = HashMap::new();
        let mut len_map = HashMap::new();
        let genesis_hash: H256 = genesis.hash();
        blk_map.insert(genesis_hash, genesis);
        len_map.insert(genesis_hash, 1);
        println!("Generate a GENESIS BLOCK {:?} !", genesis_hash);
        Blockchain{blk_map: blk_map, len_map: len_map, tip: genesis_hash}
    }

    /// Insert a block into blockchain
    pub fn insert(&mut self, block: &Block) {
        let prev = block.header.parent;
        let block_hash = block.hash();
        self.blk_map.insert(block_hash, block.clone()); // why block.clone()? expected struct `Block`, found `&Block`
        self.len_map.insert(block_hash, self.len_map[&prev]+1);
        if self.len_map[&block_hash] > self.len_map[&self.tip] {
            self.tip = block_hash;
        }
    }

    /// Get the last block's hash of the longest chain
    pub fn tip(&self) -> H256 {
        self.tip
    }

    /// Get all blocks' hashes of the longest chain, ordered from genesis to the tip
    pub fn all_blocks_in_longest_chain(&self) -> Vec<H256> {
        let mut cur = self.tip;
        let mut longest: VecDeque<H256> = VecDeque::new();
        let end = [0u8; 32].into();
        while cur != end {
            longest.push_front(cur); 
            cur = self.blk_map[&cur].header.parent;
        }
        Vec::from(longest)
    }

    pub fn all_txn_per_blk_in_longest_chain(&self) -> Vec<Vec<H256>> {
        let mut cur = self.tip;
        let mut txn_history: VecDeque<Vec<H256>> = VecDeque::new();
        let end = [0u8; 32].into();
        while cur != end {
            let mut vec_h256: Vec<H256> = Vec::new();
            for sig_txn in &self.blk_map[&cur].content.data {
                vec_h256.push(sig_txn.hash());
            }
            txn_history.push_front(vec_h256); 
            cur = self.blk_map[&cur].header.parent;
        }
        Vec::from(txn_history)
    }

    pub fn get_state_per_blk(&self, blk_num: u64, state_per_blk: &Arc<Mutex<StatePerBlock>>) -> Vec<(Address, u128, i32)> {
        // hash for block at blk_num
        let mut vec_blk_hash = Vec::new();
        vec_blk_hash = self.all_blocks_in_longest_chain();
        let target_blk_hash = vec_blk_hash[blk_num as usize];
        let mut state_per_blk_unwrap = state_per_blk.lock().unwrap();
        let state = state_per_blk_unwrap.blkStateMap.get(&target_blk_hash).unwrap();
        let mut vec_account_info = Vec::new();
        for (addr, val) in state.ledger.iter() {
            let nonce = val.0;
            let balance = val.1;
            vec_account_info.push((*addr, nonce, balance));
        }
        println!("debug in get_state_per_blk");
        vec_account_info
    }
}

// DO NOT CHANGE THIS COMMENT, IT IS FOR AUTOGRADER. BEFORE TEST

#[cfg(test)]
mod tests {
    use super::*;
    use crate::types::block::generate_random_block;
    use crate::types::hash::Hashable;

    #[test]
    fn insert_one() {
        let mut blockchain = Blockchain::new();
        let genesis_hash = blockchain.tip();
        let block = generate_random_block(&genesis_hash);
        blockchain.insert(&block);
        assert_eq!(blockchain.tip(), block.hash());

    }
}

// DO NOT CHANGE THIS COMMENT, IT IS FOR AUTOGRADER. AFTER TEST