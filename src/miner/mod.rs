pub mod worker;

use log::info;

use crossbeam::channel::{unbounded, Receiver, Sender, TryRecvError};
use std::time;

use std::thread;

use crate::types::block::{Block, Header, Content};
use crate::types::hash::Hashable;
use crate::blockchain::Blockchain;
use crate::types::merkle::MerkleTree;
use crate::types::transaction::{Transaction, SignedTransaction};
use std::sync::{Arc, Mutex};
use crate::types::hash::{H256};
use crate::types::transaction::MemPool;
use crate::types::state::{State, StatePerBlock};
use std::collections::HashSet;

enum ControlSignal {
    Start(u64), // the number controls the lambda of interval between block generation
    Update, // update the block in mining, it may due to new blockchain tip or new transaction
    Exit,
}

enum OperatingState {
    Paused,
    Run(u64),
    ShutDown,
}

pub struct Context {
    /// Channel for receiving control signal
    control_chan: Receiver<ControlSignal>,
    operating_state: OperatingState,
    finished_block_chan: Sender<Block>,
    blockchain: Arc<Mutex<Blockchain>>,
    mempool: Arc<Mutex<MemPool>>,
    state_per_blk: Arc<Mutex<StatePerBlock>>,
    state: Arc<Mutex<State>>,
    // tip: H256,
}

#[derive(Clone)]
pub struct Handle {
    /// Channel for sending signal to the miner thread
    control_chan: Sender<ControlSignal>,
}

pub fn new(
    blockchain: &Arc<Mutex<Blockchain>>, 
    mempool: &Arc<Mutex<MemPool>>, 
    state_per_blk: &Arc<Mutex<StatePerBlock>>,
    state: &Arc<Mutex<State>>,
) -> (Context, Handle, Receiver<Block>) {
    let (signal_chan_sender, signal_chan_receiver) = unbounded();
    let (finished_block_sender, finished_block_receiver) = unbounded();

    let ctx = Context {
        control_chan: signal_chan_receiver,
        operating_state: OperatingState::Paused,
        finished_block_chan: finished_block_sender,
        blockchain: Arc::clone(blockchain),
        mempool: Arc::clone(mempool),
        state_per_blk: Arc::clone(state_per_blk),
        state: Arc::clone(state),
        // tip: blockchain.lock().unwrap().tip(),
    };

    let handle = Handle {
        control_chan: signal_chan_sender,
    };

    (ctx, handle, finished_block_receiver)
}

#[cfg(any(test,test_utilities))]
fn test_new() -> (Context, Handle, Receiver<Block>) {
    let blockchain = Blockchain::new();
    let blockchain = Arc::new(Mutex::new(blockchain));
    new(&blockchain)
}

impl Handle {
    pub fn exit(&self) {
        self.control_chan.send(ControlSignal::Exit).unwrap();
    }

    pub fn start(&self, lambda: u64) {
        self.control_chan
            .send(ControlSignal::Start(lambda))
            .unwrap();
    }

    pub fn update(&self) {
        self.control_chan.send(ControlSignal::Update).unwrap();
    }
}

impl Context {
    pub fn start(mut self) {
        thread::Builder::new()
            .name("miner".to_string())
            .spawn(move || {
                self.miner_loop();
            })
            .unwrap();
        info!("Miner initialized into paused mode");
    }

    fn miner_loop(&mut self) {
        // main mining loop
        //let mut tmp_tip = self.blockchain.lock().unwrap().tip();
        // let max_txn_num = 10;
        let max_txn_num = 1;
        loop {
            // check and react to control signals
            match self.operating_state {
                OperatingState::Paused => {
                    let signal = self.control_chan.recv().unwrap();
                    match signal {
                        ControlSignal::Exit => {
                            info!("Miner shutting down");
                            self.operating_state = OperatingState::ShutDown;
                        }
                        ControlSignal::Start(i) => {
                            info!("Miner starting in continuous mode with lambda {}", i);
                            self.operating_state = OperatingState::Run(i);
                        }
                        ControlSignal::Update => {
                            // in paused state, don't need to update
                        }
                    };
                    continue;
                }
                OperatingState::ShutDown => {
                    return;
                }
                _ => match self.control_chan.try_recv() {
                    Ok(signal) => {
                        match signal {
                            ControlSignal::Exit => {
                                info!("Miner shutting down");
                                self.operating_state = OperatingState::ShutDown;
                            }
                            ControlSignal::Start(i) => {
                                info!("Miner starting in continuous mode with lambda {}", i);
                                self.operating_state = OperatingState::Run(i);
                            }
                            ControlSignal::Update => {
                                unimplemented!()
                            }
                        };
                    }
                    Err(TryRecvError::Empty) => {}
                    Err(TryRecvError::Disconnected) => panic!("Miner control channel detached"),
                },
            }
            if let OperatingState::ShutDown = self.operating_state {
                return;
            }

            // TODO for student: actual mining, create a block
            use rand::Rng;
            let mut rng = rand::thread_rng();
            let mut chain_unwrap = self.blockchain.lock().unwrap();
            let mut parent = chain_unwrap.tip();
            // let parent = self.tip; 
            // let difficulty: H256 = [255u8; 32].into();
            let difficulty = chain_unwrap.blk_map[&parent].header.difficulty;
            let timestamp = time::SystemTime::now().duration_since(time::UNIX_EPOCH).expect("Wrong order").as_millis();
            
            /// Package transactions
            let mut transactions = Vec::new();
            let mut mempool_unwrap = self.mempool.lock().unwrap();
            let mut cur_txn_num = 0;

            // mempool waiting for txns, continue to next iteration
            if mempool_unwrap.txn_map.len() == 0 {
                continue;
            }
            
            for key in mempool_unwrap.txn_map.keys() {
                let sig_txn = mempool_unwrap.txn_map[&key].clone();
                if cur_txn_num + 1 > max_txn_num {
                    break;
                }
                transactions.push(sig_txn);
                cur_txn_num += 1;
            }

            let m_tree = MerkleTree::new(&transactions);
            let merkle_root = m_tree.root();
            let nonce = rng.gen();
            let header = Header{parent: parent, nonce: nonce, difficulty: difficulty, 
                                timestamp: timestamp, merkle_root: merkle_root};
            let content = Content{data: transactions};
            let cur_blk = Block{header: header, content: content};
            // TODO for student: if block mining finished, you can have something like: self.finished_block_chan.send(block.clone()).expect("Send finished block error");
            if cur_blk.hash() <= difficulty {
                let mut state_unwrap = self.state.lock().unwrap();
                for sig_txn in cur_blk.clone().content.data {
                    mempool_unwrap.remove(&sig_txn);
                    state_unwrap.update(&sig_txn);
                }
                drop(mempool_unwrap);
                
                let mut state_per_blk_unwrap = self.state_per_blk.lock().unwrap();
                state_per_blk_unwrap.blkStateMap.insert(cur_blk.hash(), state_unwrap.clone());
                //state_per_blk_unwrap.insertState(cur_blk.hash(), state_unwrap.ledger.clone());
                
                self.finished_block_chan.send(cur_blk.clone()).expect("Send finished block error");
                drop(state_per_blk_unwrap);
                drop(state_unwrap);
                //chain_unwrap.insert(&cur_blk); // insert current block to blockchain  
                // self.tip = cur_blk.hash(); // midterm 4, we do actually insert blocks
            }
            if let OperatingState::Run(i) = self.operating_state {
                if i != 0 {
                    let interval = time::Duration::from_micros(i as u64);
                    thread::sleep(interval);
                }
            }
        }
    }
}

// DO NOT CHANGE THIS COMMENT, IT IS FOR AUTOGRADER. BEFORE TEST

#[cfg(test)]
mod test {
    use ntest::timeout;
    use crate::types::hash::Hashable;

    #[test]
    #[timeout(60000)]
    fn miner_three_block() {
        let (miner_ctx, miner_handle, finished_block_chan) = super::test_new();
        miner_ctx.start();
        miner_handle.start(0);
        let mut block_prev = finished_block_chan.recv().unwrap();
        for _ in 0..2 {
            let block_next = finished_block_chan.recv().unwrap();
            assert_eq!(block_prev.hash(), block_next.get_parent());
            block_prev = block_next;
        }
    }
}

// DO NOT CHANGE THIS COMMENT, IT IS FOR AUTOGRADER. AFTER TEST