use log::info;

use crossbeam::channel::{unbounded, Receiver, Sender, TryRecvError};
use std::time;

use std::thread;

use crate::types::block::{Block, Header, Content};
use crate::types::hash::Hashable;
use crate::blockchain::Blockchain;
use crate::types::merkle::MerkleTree;
use crate::types::transaction::{Transaction, SignedTransaction};
use std::sync::{Arc, Mutex};
use crate::types::hash::{H256};
use ring::signature::{Ed25519KeyPair, Signature, KeyPair, VerificationAlgorithm, EdDSAParameters, UnparsedPublicKey, ED25519};
use crate::types::key_pair;
use crate::types::address::{Address};
use crate::network::server::Handle as ServerHandle;
use ring::digest;
use crate::network::message::Message;
use crate::types::transaction::MemPool;
use crate::types::state::{State, StatePerBlock};
pub mod keypair_lookup;
use crate::tx_generator::keypair_lookup::KeypairMap;
use std::collections::HashSet;
use std::i64;

// use rand::distributions::uniform::{SampleUniform};

enum ControlSignal {
    Start(u64), // the number controls the lambda of interval between block generation
    Update, // update the block in mining, it may due to new blockchain tip or new transaction
    Exit,
}

enum OperatingState {
    Paused,
    Run(u64),
    ShutDown,
}

pub struct Context {
    /// Channel for receiving control signal
    control_chan: Receiver<ControlSignal>,
    operating_state: OperatingState,
    mempool: Arc<Mutex<MemPool>>,
    server: ServerHandle,
    state_per_blk: Arc<Mutex<StatePerBlock>>,
    keypair_map: KeypairMap,
    produced_set: HashSet<(Address, u128)>,
    peer_addr: String,
    blockchain: Arc<Mutex<Blockchain>>,
}

#[derive(Clone)]
pub struct Handle {
    /// Channel for sending signal to the miner thread
    control_chan: Sender<ControlSignal>,
}

pub fn new(
    mempool: &Arc<Mutex<MemPool>>, 
    server: &ServerHandle, 
    keypair_map: KeypairMap, 
    state_per_blk: &Arc<Mutex<StatePerBlock>>, 
    peer_addr: String,
    blockchain: &Arc<Mutex<Blockchain>>
) -> (Context, Handle) {
    let (signal_chan_sender, signal_chan_receiver) = unbounded();
    let produced_set = HashSet::new(); // record produced (address, nonce)
    let ctx = Context {
        control_chan: signal_chan_receiver,
        operating_state: OperatingState::Paused,
        mempool: Arc::clone(mempool),
        server: server.clone(),
        state_per_blk: Arc::clone(state_per_blk),
        keypair_map: keypair_map,
        produced_set: produced_set,
        peer_addr: peer_addr,
        blockchain: Arc::clone(blockchain),
    };

    let handle = Handle {
        control_chan: signal_chan_sender,
    };

    (ctx, handle)
}

impl Handle {
    pub fn exit(&self) {
        self.control_chan.send(ControlSignal::Exit).unwrap();
    }

    pub fn start(&self, theta: u64) {
        self.control_chan
            .send(ControlSignal::Start(theta))
            .unwrap();
    }

    pub fn update(&self) {
        self.control_chan.send(ControlSignal::Update).unwrap();
    }
}

impl Context {
    pub fn start(mut self) {
        thread::Builder::new()
            .name("tx-generator".to_string())
            .spawn(move || {
                self.tx_generator_loop();
            })
            .unwrap();
        info!("tx-generator initialized into paused mode");
    }

    fn tx_generator_loop(&mut self) {
        // main tx-generator loop
        // let mut tmp_nonce_val: u128 = 0;
        loop {
            // check and react to control signals
            match self.operating_state {
                OperatingState::Paused => {
                    let signal = self.control_chan.recv().unwrap();
                    match signal {
                        ControlSignal::Exit => {
                            info!("tx-generator shutting down");
                            self.operating_state = OperatingState::ShutDown;
                        }
                        ControlSignal::Start(i) => {
                            info!("tx-generator starting in continuous mode with theta {}", i);
                            self.operating_state = OperatingState::Run(i);
                        }
                        ControlSignal::Update => {
                            // in paused state, don't need to update
                        }
                    };
                    continue;
                }
                OperatingState::ShutDown => {
                    return;
                }
                _ => match self.control_chan.try_recv() {
                    Ok(signal) => {
                        match signal {
                            ControlSignal::Exit => {
                                info!("tx-generator shutting down");
                                self.operating_state = OperatingState::ShutDown;
                            }
                            ControlSignal::Start(i) => {
                                info!("tx-generator starting in continuous mode with theta {}", i);
                                self.operating_state = OperatingState::Run(i);
                            }
                            ControlSignal::Update => {
                                unimplemented!()
                            }
                        };
                    }
                    Err(TryRecvError::Empty) => {}
                    Err(TryRecvError::Disconnected) => panic!("tx-generator control channel detached"),
                },
            }
            if let OperatingState::ShutDown = self.operating_state {
                return;
            }

            // TODO for student: actual generation, create a transaction
            use rand::Rng;
            let mut rng = rand::thread_rng();
            // let k_p = key_pair::random();
            // let public_key = k_p.public_key(); // how to retrieve public key from address/state's key ???
            // // get the following three vars from state
            // let sender: Address = Address(rng.gen::<[u8; 20]>());
            // let receiver: Address = Address(rng.gen::<[u8; 20]>());
            // let value: i32 = rng.gen();
            let tip = self.blockchain.lock().unwrap().tip();
            let mut state_per_blk_unwrap = self.state_per_blk.lock().unwrap(); // use weird hashmap to get the state of the blkchain tip
            println!("tip hash : {:?}", tip);
            let mut state_unwrap = &state_per_blk_unwrap.blkStateMap[&tip];
            
            drop(tip);
            let mut keys: Vec<Address> = Vec::new(); // use "keys" to store all possible sender address
            for k in state_unwrap.ledger.keys(){
                keys.push(*k);
            }
            let mut low = 0;
            let mut high = keys.len();
            

            let sen_idx = rng.gen_range(low..high);
            let sender: Address = keys[sen_idx];
            let mut send_idx_in_db_vec = hex::decode(sender.to_string()).expect("Decoding failed");
            let mut send_idx_in_db = send_idx_in_db_vec[19];
            //println!("send_idx_in_db_vec = {:?}", send_idx_in_db_vec);
            println!("--------> trying sender idx in db:{:?}", send_idx_in_db);

            if self.peer_addr == "127.0.0.1:6000" && send_idx_in_db > 32 {
                continue;
            } 
            else if self.peer_addr == "127.0.0.1:6001" && (send_idx_in_db < 33 || send_idx_in_db > 64) {
                continue;
            } 
            else if self.peer_addr == "127.0.0.1:6002" && send_idx_in_db < 65 {
                continue;
            }

            let sender_kp = &self.keypair_map.info[&sender];
            let new_nonce = state_unwrap.ledger[&sender].0 + 1; // new_nonce won't be incremented since state is now updated per block, not per txn
            
            let cur_balance = state_unwrap.ledger[&sender].1;
            if cur_balance == 0 {
                println!("cur_balance <--------------000000000000000000000000000000000000000000b");
                continue;
            }
            let mut value = rng.gen_range(0..cur_balance);
            if value == 0 {
                //println!("cur_balance <--------------000000000000000000000000000000000000000000val");
                //continue;
                value = 1;
            }
            let mut rcv_idx = rng.gen_range(0..100);
            while rcv_idx == sen_idx {
                rcv_idx = rng.gen_range(0..100);
            }
            let mut rcv = [0u8; 20];
            rcv[19] = rcv_idx as u8;
            let receiver: Address = Address(rcv);
            
            // DEBUG: move if-else down below value == 0 check
            if self.produced_set.contains(&(sender, new_nonce)){
                if send_idx_in_db == 0 {
                    println!("Stuck in produced_set.contains <-------------------------{:?}, {:?}", send_idx_in_db, new_nonce);
                }
                continue;
            } else {
                self.produced_set.insert((sender, new_nonce)); // moved down here b.c. if we check if value == 0 first and then continue -> insert (sender, nonce) but actually transaction was not generated -> stuck in produced set check
                println!("insert (sender, nonce) into produced_set");
            }

            let txn = Transaction{sender: sender, receiver: receiver, value: value, nonce: new_nonce}; // nonce's should be adjust in later MP
            println!("receiver address: {:?}", receiver);
            println!("a txn sender: {:?}, receiver: {:?}, value: {:?}, nonce: {:?} is produced.", sender, receiver, value, new_nonce);
            // tmp_nonce_val += 1;
            let tmp = bincode::serialize(&txn).unwrap();
            let txn_id = digest::digest(&digest::SHA256, digest::digest(&digest::SHA256, tmp.as_ref()).as_ref());
            let signature = sender_kp.sign(txn_id.as_ref());
            let sig_txn = SignedTransaction{transaction: txn, signature: signature.as_ref().to_vec(), public_key: sender_kp.public_key().as_ref().to_vec()};
            
            let mut mempool_unwrap = self.mempool.lock().unwrap();
            // since it is generated by local process (and inserted into local mempool), not need to verify
            mempool_unwrap.insert(&sig_txn);
            drop(mempool_unwrap); // drop mutex lock
            let mut hash: H256 = sig_txn.hash();
            self.server.broadcast(Message::NewTransactionHashes(vec![hash]));
            if let OperatingState::Run(i) = self.operating_state {
                if i != 0 {
                    let interval = time::Duration::from_micros(i as u64);
                    thread::sleep(interval);
                }
            }
        }
    }
}