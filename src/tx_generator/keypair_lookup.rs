use serde::{Serialize,Deserialize};
use std::collections::{HashMap, BTreeMap};
use ring::signature::{Ed25519KeyPair, Signature, KeyPair, UnparsedPublicKey, ED25519};
use crate::types::address::{Address};
use rand::SeedableRng;
use ring::rand::SecureRandom;
use std::fs::{File};
use std::fs;
use std::io::prelude::*;
use std::path::Path;
use serde_json::{Result, json, to_writer, from_reader};

// #[derive(Serialize, Deserialize, Debug, Default, Clone)]
#[derive(Debug, Default)]
pub struct KeypairMap {
    pub info: HashMap<Address, Ed25519KeyPair>,
}

impl KeypairMap{
    pub fn new() -> Self {
        let mut info = HashMap::new();
        // Read file and construct the keypair map
        let data = fs::read_to_string("../../addr_pkcs8.json").expect("Unable to read file"); // current directory is /target/debug 
        let json: serde_json::Value = serde_json::from_str(&data).expect("JSON was not well-formatted");

        for data in json.as_array().unwrap(){
            let mut tmp_addr = [0u8; 20];
            for i in 0..20 {
                tmp_addr[i] = data["address"][i].as_u64().unwrap() as u8;
            }
            let addr = Address(tmp_addr);
            let mut tmp_pkcs8 = [0u8; 85];
            for i in 0..85 {
                tmp_pkcs8[i] = data["pkcs8"][i].as_u64().unwrap() as u8;
            }
            let kp = Ed25519KeyPair::from_pkcs8(&tmp_pkcs8).unwrap();
            info.insert(addr, kp);
        }
        KeypairMap{info: info}
    }
}

// let mut init_vec = [0u8; 20];
// for i in 0 .. 100 {
//     // let seed = rand::SystemRandom::new();
//     let seed: SecureRandom = SeedableRng::seed_from_u64(42);
//     let pkcs8_bytes = Ed25519KeyPair::generate_pkcs8(&seed).unwrap(); // cannot take int
//     let k_p = Ed25519KeyPair::from_pkcs8(pkcs8_bytes.as_ref().into()).unwrap();
//     if i < 10 {
//         println!("{:?}", hex::encode(k_p.public_key().as_ref().to_vec()));
//     }
//     let addr: Address = Address(init_vec);
//     init_vec[19] += 1;
//     // let addr = Address.from_public_key_bytes(hex::encode(k_p.public_key().as_ref().to_vec()));
//     info.insert(addr, k_p);
// }
